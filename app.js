const express = require("express");
const path = require('path');
const mysql = require("mysql");
const dotenv = require('dotenv');
const cookieParser = require('cookie-parser');

dotenv.config({ path: './.env'});
const PORT = process.env.PORT || 3005; // creating a port where our server should start


const app = express();

//connect to database
const db = mysql.createConnection ({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE
})

db.connect( (error) => {
    if(error) {
        console.log(error)
    } else {
        console.log("MYSQL Connected...")
    }
});

//Public Directories
const publicDirectory = path.join(__dirname, './public');
app.use(express.static(publicDirectory));

//Parse url encoded bodies
app.use(express.urlencoded({ extended: false }));
//Parse JSON bodies
app.use(express.json());

app.use(cookieParser());

//setting view engine
app.set('view engine', 'hbs');

//Routes defined
app.use('/', require('./routes/pages'));
//app.use('/', require('./routes/api'));
app.use('/auth', require('./routes/auth'));


app.listen(7000, () => {
    console.log("server running on Port 7000")
});