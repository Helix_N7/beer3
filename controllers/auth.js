const mysql =require("mysql");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const cookieParser = require('cookie-parser');
const { promisify } = require('util');

const db = mysql.createConnection ({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE
})

//LOGIN
exports.login = async(req,res) =>{
    try {
        const { name , password } = req.body ;
        if( !name || !password ){
            return res.status(400).render('login' , {
                message : 'Please provide and name and password'
            })
        }

        db.query('SELECT * FROM users WHERE name = ?' , [name] , async(error,result) => {
            console.log(result)
            if( !result || result.length == 0 || !(await bcrypt.compare(password , result[0].password )) ){
                res.status(401).render('login' ,{
                    message : 'email or Password is incorrect'
                })
            }
            else{
                const id = result[0].id;
                //creating a token
                const token  = jwt.sign({ id : id } , process.env.JWT_SECRET , {
                    expiresIn : process.env.JWT_EXPIRES_IN
                });

                console.log("The token is : " + token );

                // //when does our token expires
                const cookieParser = {
                    expiresIn : new Date(
                        Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000
                    ),
                    //just to prevent if someone is not messing with our cookies 
                    httpOnly : true 
                }
                //we can use any name here in 
                res.cookie(name , token , cookieParser ) ;
                //after a user is loged in we put cookie in browser
                res.cookie('jwt' , token );
                res.status(200).redirect('/');
            }
        })
    } catch (error) {
        console.log(error);
    }
}

//REGISTER
exports.register = (req, res) => {
    console.log(req.body);

    const { name, email, password, passwordConf } = req.body;
    db.query('Select email FROM users WHERE email = ?', [email], async (error, results) => {
        if(error){
            console.log(error);
        }
        if( results.length > 0 ){
            return res.render('register', {
                message: 'Email already in use'
            })
        } else if ( password !== passwordConf ) {
            return res.render('register', {
                message: 'Passwords does not match'
            });
        }

        let hashedPassword = await bcrypt.hash(password, 8);
        console.log(hashedPassword);

        db.query('INSERT INTO users SET ?', {name: name, email: email, password: hashedPassword }, (error, results) => {
            if(error) {
                console.log(error);
            } else {
                console.log(results)
                return res.render('register', {
                    message: 'User registered'
                });
            }
        })
    });
}

//USER LOGGED IN
exports.isLoggedIn = async(req,res,next) => {
    console.log(req.cookies);
    if( req.cookies.jwt ){
        try {
            //step 1 : Verify the token
            const decoded = await promisify(jwt.verify)(
                req.cookies.jwt , 
                process.env.JWT_SECRET
                )
                console.log(decoded);

            //step 2: check if the user still exists 
            db.query('SELECT * FROM users WHERE id = ?' , [decoded.id] , (error,result) => {
                console.log(result);

                if( !result ){
                    return next();
                }
                req.user = result[0];
                return next();
            });

        } catch (error) {
           console.log(error);
           return next();
        }
    }
    else{
        next();
    }
}

exports.logout = async( req , res ) => {
    res.cookie('jwt' , 'logout' , {
        expires : new Date(Date.now() + 2*1000 ) ,
        httpOnly: true
    });
    res.status(200).redirect('/login');
}