const express = require('express');
const authController = require('../controllers/auth')
const mysql =require("mysql");
const cors = require('cors');
const router = express.Router();
const app = express();


const db = mysql.createConnection ({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE
})


// // creating a root so to check if its send response to backend or not
router.get('/' , authController.isLoggedIn , (req,res)=>{
  db.query('SELECT * FROM beers WHERE like_count > 1',(err, results) => {
      if (err) {
        throw err;
      }
      res.render("index" , { user_details : req.user, data : results })
  });
});

//register page
router.get('/register', (req, res) => {
    res.render('register');
});
router.get('/login', (req, res) => {
    res.render('login');
});

router.get('/profile' , authController.isLoggedIn ,  (req,res)=>{
    console.log(req.user);

    if( req.user ){
        res.render('profile' , {
            user_details : req.user
        })
    }
    else{
        res.redirect('/login');
    }
})

//FETCH API

//view 10 beers
router.get('/', (req, res) => {
  // Fetch data from MySQL
  db.query('SELECT * FROM beers = 10', (err, results) => {
    if (err) {
      throw err;
    }
    // Render data using Handlebars
    res.render('beers', { beers : results });
  });
});

//view all beers
router.get('/beers', authController.isLoggedIn , (req, res) => {
    db.query('SELECT * FROM beers', (err, results) => {
      if (err) throw err;
      res.render('beers', { user_details : req.user, Data: results });
    });
  });

//view beer by ID
router.get('/beer', (req, res) => {
  db.query('SELECT * FROM beers WHERE id = ?', (err, results) => {
    if (err) throw err;
    res.render('beer', { beer: results });
  });
});


// Endpoint to handle like button click
app.post('Like-beer', authController.isLoggedIn , (req, res) => {
  const id = req.body.id;
  // Increment likes for the given item
  db.query('UPDATE beers SET like_cont = like_count + 1 WHERE id = ?', id, (error, results) => {
      if (error) throw error;
      res.send('Item liked successfully');
  });
});

// // Get logged in user
router.get('/users', authController.isLoggedIn, (req, res) => {
    db.query('SELECT * FROM users WHERE id = ?', (err, results) => {
      if (err) throw err;
      res.json(results);
    });
  });

  //CORS OPERATIONS

  var corsOptions = {
    origin: '*',
  }

  app.get('/users/:id', cors(corsOptions), function (req, res, next) {
    res.json({msg: 'This is CORS-enabled for only example.com.'})
  })


module.exports =router;